package model;

public class Programare extends Curs{
    
    public Programare(String nume, int pret){
        super(nume, pret);
    }
    
    @Override
    public String toString(){
        return "Cursul de programare " + this.getNume() + "are pretul: " + 
                this.getPret() + "si id-ul: " + this.getId();
        
    }
}