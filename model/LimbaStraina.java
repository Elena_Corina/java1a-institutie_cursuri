package model;

public class LimbaStraina extends Curs{
    
        public LimbaStraina(String nume, int pret){
        super(nume, pret);
    }
    
    @Override
    public String toString(){
        return "Cursul de limbi straine " + this.getNume() + "are pretul: " + 
                this.getPret() + "si id-ul: " + this.getId();
        
    }
}