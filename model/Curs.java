package model;

public class Curs{
    private int id;
    private String nume;
    private int pret;
    private static int countId =1;
    
    public Curs(String nume, int pret){
        this.id = Curs.countId;
        this.nume = nume;
        this.pret = pret;
        Curs.countId++;
    }
    
    public void setNume(String nume){
        this.nume = nume;
    }
    
    public void setPret(int pret){
        this.pret = pret;
    }
    
    public String getNume(){
        return this.nume;
    }
    
    public int getPret(){
        return this.pret;
    }
    
    public int getId(){
        return this.id;
    }
}