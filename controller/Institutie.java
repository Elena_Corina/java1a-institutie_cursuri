package controller;

import model.Curs;
import model.Programare;

import java.util.ArrayList;

public class Institutie{
    
    private final int nrMaxCursuri = 4;
    private ArrayList<Curs> cursuri = new ArrayList<Curs>();
    private static Institutie instance = null;
    
    private Institutie(){}
    
    public static Institutie getInstance(){
        if(instance == null){
            instance = new Institutie();
        }
        return instance;
    }
    
    public void adaugaCurs(Curs c) throws PreaMulteCursuriException{
        if(cursuri.size() < nrMaxCursuri){
            cursuri.add(c);
        } else{
            throw new PreaMulteCursuriException();
        }
    }
    
    public void afiseazaCursuri(){
        for(Curs c: cursuri){
            System.out.println(c);
        }
    }
    
    public void afiseazaCursuriProgramare(){
        for(int i = 0; i<cursuri.size(); i++){
            if(cursuri.get(i) instanceof Programare){
                System.out.println(cursuri.get(i));
            }
        }
    }
    
    public void stergeCurs(int id){
        for(int i=0; i<cursuri.size(); i++){
            if(cursuri.get(i).getId() == id){
                cursuri.remove(cursuri.get(i));
            }
        }
    } 
    
    public int getSize(){
        return cursuri.size();
    }
}