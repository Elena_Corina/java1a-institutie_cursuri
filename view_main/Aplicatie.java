package view_main;

import controller.Institutie;
import controller.NumarCursuri;
import controller.PreaMulteCursuriException;
import model.LimbaStraina;
import model.Programare;

import java.util.*;


public class Aplicatie{
    public static void main(String[] args) {
        
        NumarCursuri nr = new NumarCursuri();
        Thread t = new Thread(nr);
        t.start();
    
        Institutie scoala = Institutie.getInstance();
        
        Scanner sc = new Scanner(System.in);
        
        String line = "";
        
        while (true){
            line = sc.nextLine();
            String [] cmd = line.split("\\s+");
            
            switch(cmd[0]){
                case "adaugaCurs": 
                    String tip = cmd[1];
                    String nume = cmd[2];
                    int pret = Integer.parseInt(cmd[3]);
                    
                    try{
                        if(tip.equals("programare")){
                            Programare p = new Programare(nume, pret);
                            scoala.adaugaCurs(p);
                            System.out.println("s-a adaugat un curs de programare");
                        }else if (tip.equals("limba")){
                            LimbaStraina ls = new LimbaStraina(nume, pret);
                            scoala.adaugaCurs(ls);
                            System.out.println("s-a adaugat un curs de limbi straine");
                        }
                    } catch(PreaMulteCursuriException e){
                        System.out.println(e.getMessage());
                    }
                break;
                case "afisare":
                    scoala.afiseazaCursuri();
                break;
                case "afCursuriProgramare":
                    scoala.afiseazaCursuriProgramare();
                break;
                case "stergeCurs":
                    int index = Integer.parseInt(cmd[1]);
                    scoala.stergeCurs(index);  
                break;
                case "exit":
                System.exit(0);   
            }
        }
    
    }
}