Cerinte Java 1 A - Institutie cursuri
 
Într-o instituție se oferă două tipuri de cursuri: cursuri de programare și cursuri de limbi străine.
Orice curs are un id, o denumire și un preț. Id-ul este un număr care corespunde numărului de ordine al cursului adăugat.
Instituția poate oferi maxim 30 de cursuri (de programare și de limbi straine) și are următoarele comportamente:
- adaugăCurs - care adaugă un nou curs în oferta de cursuri 
(daca mai exista spatiu, în caz contrar se va arunca o excepție de tipul PreaMulteCursuriException – definită de programator);
- afișeazăCursuri – afișează toată oferta de cursuri disponibile (cu toate detaliile acestora);
- afișeazăCursuriProgramare – afișeaza doar cursurile de programare disponibile (cu toate
caracteristicile acestora);
- stergeCurs – sterge un curs identificat prin id-ul primit ca parametru.

Aplicația primește următoarele comenzi din consolă și lucrează cu o singură instituție:
- exit – închide aplicația; (0.5p)
- adauga_curs <tip> <denumire> <preț> - adaugă un curs de tipul corespunzător în oferta
instituției; (1p)
- afișare – afișează toate cursurile din instituție; (2p)
- afisare_cursuri_programare – afișează toate cursurile de programare disponibile; (1p)
- șterge_curs <id_curs> - sterge cursul al cărui id este trimis ca parametru. (1p)
- În background va rula un fir de execuție care va afișa numărul total de cursuri. Acest thread se
va actualiza o dată la 2 minute. (1.5p)
Observații:
1. Clasele vor respecta principiul de encapsulare. Realizarea corectă a arhitecturii claselor (3 p)
2. Se va folosi Singleton Pattern acolo unde este necesar.
3. Se vor folosi stream-uri și expresii lambda oriunde este posibil.(în funcțiile de afișare, precum
și în realizarea firului de execuție)
